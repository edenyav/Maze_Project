package View;

import Server.*;
import Model.IModel;
import Model.MyModel;
import Server.Server;
import ViewModel.MyViewModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {

    private static IModel model= new MyModel();
    private static MyViewModel vm = new MyViewModel(model);
    private static Server mazeGeneratingServer,solveSearchProblemServer;
    public static Stage stage;
    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        model.addObserver(vm);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MyView.fxml"));
       // FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Test.fxml"));
        Parent root = (Parent) loader.load();
        IView myView = loader.getController();
        myView.setViewModel(vm);
        vm.addObserver(myView);

        primaryStage.setTitle("Board Game");
        Screen screen = Screen.getPrimary();
        double height = screen.getVisualBounds().getHeight()-50;
        double width = screen.getVisualBounds().getWidth();
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> {
            exitProgram();
        });
    }

    public static void MazeSettingWindow() throws IOException {

        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/View/MazeSettingWindow.fxml"));
        Parent root = (Parent) loader.load();
        IView mazeView = loader.getController();
        mazeView.setViewModel(vm);
        vm.addObserver(mazeView);

        Stage window = new Stage();
        window.setTitle("Maze Settings");
        //root = FXMLLoader.load(Main.class.getResource("/View/MazeSettingWindow.fxml"));
        window.setScene(new Scene(root, 600, 600));
        window.show();
    }

    public static void main(String[] args) {
        mazeGeneratingServer = new Server(5400, 1000, new ServerStrategyGenerateMaze());
        solveSearchProblemServer = new Server(5401, 1000, new ServerStrategySolveSearchProblem());

        //Starting  servers
        solveSearchProblemServer.start();
        mazeGeneratingServer.start();

        launch(args);
    }

    public static void closeServers(){
        solveSearchProblemServer.stop();
        mazeGeneratingServer.stop();
        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        solveSearchProblemServer = null;
        mazeGeneratingServer = null;
    }

    public static void exitProgram(){
        if(!ConfirmBox.display("Exit","Are you sure you want to exit?"))
            return;
        closeServers();
        System.exit(0);

    }

    public static void restartServers(){
        closeServers();
        mazeGeneratingServer = new Server(5400, 1000, new ServerStrategyGenerateMaze());
        solveSearchProblemServer = new Server(5401, 1000, new ServerStrategySolveSearchProblem());
        solveSearchProblemServer.start();
        mazeGeneratingServer.start();
    }
}
