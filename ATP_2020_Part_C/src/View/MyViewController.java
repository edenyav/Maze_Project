package View;

import Model.FinishException;
import Server.Configurations;
import ViewModel.MyViewModel;
import algorithms.mazeGenerators.Maze;
import algorithms.mazeGenerators.MyMazeGenerator;
import algorithms.mazeGenerators.Position;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Observable;
import java.util.ResourceBundle;


public class MyViewController extends AView implements Initializable {

    private boolean hide_solution = false;
    private boolean mute_music = true;

    private Stage window = Main.stage;

    @FXML
    public MazeDisplayer mazeDisplayer;
    @FXML
    public String PlayerMoveSound;
    @FXML
    public Label instruction;
    @FXML
    public BorderPane borderpane_main_scene;
    @FXML
    public Pane game_pane;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        borderpane_main_scene.prefHeightProperty().bind(window.heightProperty());
        borderpane_main_scene.prefWidthProperty().bind(window.widthProperty());
        game_pane.prefWidthProperty().bind(borderpane_main_scene.prefHeightProperty());
        game_pane.prefHeightProperty().bind(borderpane_main_scene.prefHeightProperty());
    }

    public void handleMenuNew() throws IOException {
        mazeDisplayer.widthProperty().bind(mazeDisplayer.getScene().widthProperty());
        mazeDisplayer.heightProperty().bind(mazeDisplayer.getScene().heightProperty());
        instruction.setText("");
        Main.MazeSettingWindow();
    }

    public void handleMenuLoad() {
        mazeDisplayer.widthProperty().bind(mazeDisplayer.getScene().widthProperty());
        mazeDisplayer.heightProperty().bind(mazeDisplayer.getScene().heightProperty());
        FileHandleWindow fw = new FileHandleWindow(viewModel,false);
        fw.loadTextFile();
    }

    public void handleMenuSave() {
        FileHandleWindow fw = new FileHandleWindow(viewModel,true);
        if(fw == null)
            showAlert("Please save the maze to a file or exit");
        fw.saveTextToFile();
    }


    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Maze)
            mazeDisplayer.drawMaze((Maze)arg);
        else if(arg instanceof int[]){
            int[] player_position = (int[])arg;
            mazeDisplayer.set_player_position(player_position[0],player_position[1]);
        }
        // TODO: Delete at the end if not needed
        else if (arg instanceof Position[]){
            Position[] start_and_goal = (Position[])arg;
            Position start = start_and_goal[0];
            Position goal = start_and_goal[1];
            mazeDisplayer.setStart_point(start);
            mazeDisplayer.setGoal_point(start);
        }

    }


    @Override
    public void keyPressed(KeyEvent keyEvent) {
        try{
            switch (keyEvent.getCode()){
                case H:
                    if(!hide_solution) {
                        showSolution(new ActionEvent());
                        hide_solution = true;
                    }
                    else {
                        hideSolution(new ActionEvent());
                        hide_solution = false;
                    }
                    break;

                case M:
                    if(mute_music) {
                        mazeDisplayer.muteBackgroundMusic();
                        mute_music = false;
                    }
                    else{
                        mazeDisplayer.playBackgroundMusic();
                        mute_music = true;
                    }
                default: // Don't move player
                    play_audio(PlayerMoveSound);
                    viewModel.updatePlayerPosition(keyEvent);
            }
        }
        catch (FinishException f){
            gameWon();
        }
        keyEvent.consume();
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        mazeDisplayer.requestFocus();
    }

    public void gameWon(){
        mazeDisplayer.muteBackgroundMusic();
        play_audio("./resources/Music/FinishMusic.mp3");
        showAlert("YOU WON!");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Ask the user if he wants to play again and if not exit the program
        if(ConfirmBox.display("YOU WON!","Play Again?")) {
            try {
                handleMenuNew();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            handleMenuExit();
    }

    @Override
    public void showSolution(ActionEvent actionEvent) {
        try {
            ArrayList<String> s = viewModel.showSolutionViewModel(actionEvent);
            mazeDisplayer.showSolution(s);
        }
        catch (NullPointerException n){
            showAlert("Please create or load a game before pushing the button");
        }
    }

    @Override
    public void hideSolution(ActionEvent actionEvent) {
        mazeDisplayer.hideMazeSolution();
    }

}
