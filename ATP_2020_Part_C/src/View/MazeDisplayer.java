package View;


import algorithms.mazeGenerators.Maze;
import algorithms.mazeGenerators.Position;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Screen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.nio.file.Paths;
import java.util.ArrayList;


public class MazeDisplayer extends Canvas {

    private Maze maze;
    private Position start_point;
    private Position goal_point;
    private int row_player,col_player;
    private double screen_height,screen_width;
    private double canvasHeight,canvasWidth,cellHeight,cellWidth;
    private Screen screen;

    private Image iWall,iBackround,iPlayer,iFinsish,iHint;
    private Media media = new Media(Paths.get("./resources/Music/BackRoundMusic.mp3").toUri().toString());
    private MediaPlayer musicplayer = new MediaPlayer(media);
    private Image iLeft,iRight,iUp,iDown,iRight_Up,iRight_Down,iLeft_Up,iLeft_Down;


    StringProperty imageFileNameWall = new SimpleStringProperty();
    StringProperty imageFileNamePlayer = new SimpleStringProperty();
    StringProperty imageFileNameGoal = new SimpleStringProperty();
    StringProperty imageFileNameHint = new SimpleStringProperty();

    StringProperty imageArrowUp = new SimpleStringProperty();
    StringProperty imageArrowUpRight = new SimpleStringProperty();
    StringProperty imageArrowRight = new SimpleStringProperty();
    StringProperty imageArrowDownRight = new SimpleStringProperty();
    StringProperty imageArrowDown = new SimpleStringProperty();
    StringProperty imageArrowDownLeft = new SimpleStringProperty();
    StringProperty imageArrowLeft = new SimpleStringProperty();
    StringProperty imageArrowUpLeft = new SimpleStringProperty();
    StringProperty imageBackround = new SimpleStringProperty();

    GraphicsContext graphicsContext;

    public MazeDisplayer() {

        widthProperty().addListener(evt -> draw());
        heightProperty().addListener(evt -> draw());;
    }

    public String getImageArrowUpRight() {
        return imageArrowUpRight.get();
    }

    public String getImageArrowRight() {
        return imageArrowRight.get();
    }

    public String getImageArrowDownRight() {
        return imageArrowDownRight.get();
    }

    public String getImageArrowDown() {
        return imageArrowDown.get();
    }

    public String getImageFileNameHint() {
        return imageFileNameHint.get();
    }

    public StringProperty imageFileNameHintProperty() {
        return imageFileNameHint;
    }

    public String getImageArrowDownLeft() {
        return imageArrowDownLeft.get();
    }

    public String getImageArrowLeft() {
        return imageArrowLeft.get();
    }

    public String getImageArrowUpLeft() {
        return imageArrowUpLeft.get();
    }

    public String getImageBackround() {
        return imageBackround.get();
    }

    public StringProperty imageBackroundProperty() {
        return imageBackround;
    }

    public void setImageFileNameHint(String imageFileNameHint) {
        this.imageFileNameHint.set(imageFileNameHint);
    }

    public void setImageArrowUp(String imageArrowUp) {
        this.imageArrowUp.set(imageArrowUp);
    }

    public void setImageArrowUpRight(String imageArrowUpRight) {
        this.imageArrowUpRight.set(imageArrowUpRight);
    }

    public void setImageArrowRight(String imageArrowRight) {
        this.imageArrowRight.set(imageArrowRight);
    }

    public void setImageArrowDownRight(String imageArrowDownRight) {
        this.imageArrowDownRight.set(imageArrowDownRight);
    }

    public void setImageArrowDown(String imageArrowDown) {
        this.imageArrowDown.set(imageArrowDown);
    }

    public void setImageArrowDownLeft(String imageArrowDownLeft) {
        this.imageArrowDownLeft.set(imageArrowDownLeft);
    }

    public void setImageArrowLeft(String imageArrowLeft) {
        this.imageArrowLeft.set(imageArrowLeft);
    }

    public void setImageArrowUpLeft(String imageArrowUpLeft) {
        this.imageArrowUpLeft.set(imageArrowUpLeft);
    }

    public void setImageBackround(String imageBackround) {
        this.imageBackround.set(imageBackround);
    }

    public String getImageArrowUp() {
        return imageArrowUp.get();
    }

    public StringProperty imageArrowUpProperty() {
        return imageArrowUp;
    }

    public String getImageFileNameGoal() {
        return imageFileNameGoal.get();
    }

    public StringProperty imageFileNameGoalProperty() {
        return imageFileNameGoal;
    }

    public void setImageFileNameGoal(String imageFileNameGoal) {
        this.imageFileNameGoal.set(imageFileNameGoal);
    }

    public String getImageFileNameWall() {
        return imageFileNameWall.get();
    }

    public void setImageFileNameWall(String imageFileNameWall) {
        this.imageFileNameWall.set(imageFileNameWall);
    }

    public String getImageFileNamePlayer() {
        return imageFileNamePlayer.get();
    }

    public void setImageFileNamePlayer(String imageFileNamePlayer) {
        this.imageFileNamePlayer.set(imageFileNamePlayer);
    }

    public int getRow_player() {
        return row_player;
    }

    public int getCol_player() {
        return col_player;
    }

    public Position getStart_point() {
        return start_point;
    }

    public void setStart_point(Position start_point) {
        this.start_point = start_point;
    }

    public Position getGoal_point() {
        return goal_point;
    }

    public void setGoal_point(Position goal_point) {
        this.goal_point = goal_point;
        draw();
    }

    public void set_player_position(int row, int col){
        this.row_player = row;
        this.col_player = col;
        draw();
    }

    public void init_player_position(int row, int col){
        this.row_player = row;
        this.col_player = col;
    }

    public void drawMaze(Maze maze) {

        this.maze = maze;
        start_point = maze.getStartPosition();
        goal_point = maze.getGoalPosition();
        init_player_position(start_point.getRowIndex(),start_point.getColumnIndex());
        try {
            iWall = new Image(new FileInputStream(getImageFileNameWall()));
            iBackround = new Image(new FileInputStream(getImageBackround()));
            iFinsish = new Image(new FileInputStream(getImageFileNameGoal()));
            iPlayer = new Image(new FileInputStream(getImageFileNamePlayer()));
            iHint = new Image(new FileInputStream(getImageFileNameHint()));

        } catch (FileNotFoundException e) {
            System.out.println("There is no Image file...." + e.getMessage());
        }
        playBackgroundMusic();
        draw();
    }

    public void draw()
    {
        screen = Screen.getPrimary();
        screen_height = screen.getVisualBounds().getHeight()-50;
        screen_width = screen.getVisualBounds().getWidth()-20;
       // System.out.println(screen_width + "");
       // setHeight(screen_height);
      //  setWidth(screen_width);
        if( maze!=null)
        {
           // double canvasHeight = screen_height;
            canvasHeight = getHeight();
            canvasWidth = getWidth();
            System.out.println(canvasHeight + " Height");
            System.out.println(canvasWidth + " Width");

            // double canvasWidth = screen_width;
            int row = maze.getRows();
            int col = maze.getColumns();

            cellHeight = canvasHeight/row;
            cellWidth = canvasWidth/col;
            graphicsContext = getGraphicsContext2D();

            graphicsContext.clearRect(0,0,canvasWidth,canvasHeight);
            graphicsContext.setFill(Color.RED);
            //Draw Maze
            for(int i=0;i<row;i++)
                for(int j=0;j<col;j++)
                    if(!maze.checkCell(i,j)) // Wall
                        drawImage(i,j,getImageFileNameWall(),iWall);
                    else // Backround
                        drawImage(i,j,getImageBackround(),iBackround);

            drawStartAndGoal();
        }
    }

    public void showSolution(ArrayList<String> solution) {
        if (graphicsContext == null)
            return;

        int current_row = start_point.getRowIndex();
        int current_col = start_point.getColumnIndex();
        String direction = "";
        for (int i = 0; i < solution.size();i++) {
            direction = solution.get(i);
            switch (direction) {
                case "UP":
                    drawImage(current_row,current_col,getImageArrowUp(),iHint);
                    current_row = current_row-1;
                    break;
                case "UP_RIGHT":
                    drawImage(current_row,current_col,getImageArrowUpRight(),iHint);
                    current_row = current_row-1;
                    current_col = current_col +1;
                    break;
                case "RIGHT":
                    drawImage(current_row,current_col,getImageArrowRight(),iHint);
                    current_col = current_col +1;
                    break;
                case "DOWN_RIGHT":
                    drawImage(current_row,current_col,getImageArrowDownRight(),iHint);
                    current_row = current_row+1;
                    current_col = current_col+1;
                    break;
                case "DOWN":
                    drawImage(current_row,current_col,getImageArrowDown(),iHint);
                    current_row = current_row+1;
                    break;
                case "DOWN_LEFT":
                    drawImage(current_row,current_col,getImageArrowDownLeft(),iHint);
                    current_row = current_row+1;
                    current_col = current_col-1;
                    break;
                case "LEFT":
                    drawImage(current_row,current_col,getImageArrowLeft(),iHint);
                    current_col = current_col-1;
                    break;
                case "UP_LEFT":
                    drawImage(current_row,current_col,getImageArrowUpLeft(),iHint);
                    current_row = current_row-1;
                    current_col = current_col-1;
                    break;
            }
        }
    }

    public void hideMazeSolution(){draw();}

    public void drawStartAndGoal(){
        // Draw the start position with the current player
        drawImage(row_player,col_player,getImageFileNamePlayer(),iPlayer);

        // Draw the goal position
        drawImage(goal_point.getRowIndex(),goal_point.getColumnIndex(),getImageFileNameGoal(),iFinsish);
    }

    public void drawImage(double x_position,double y_poisiton,String filepath,Image image){
        double x = x_position * cellHeight;
        double y = y_poisiton * cellWidth;
       // Image image = null;
        try {
            if(image == null)
                image = new Image(new FileInputStream(filepath));
            graphicsContext.drawImage(image,y,x,cellWidth,cellHeight);
        } catch (FileNotFoundException e) {
            System.out.println("There is no Image file....");
            graphicsContext.fillRect(y,x,cellWidth,cellHeight);

        }
    }
    public void playBackgroundMusic(){
        musicplayer.setVolume(0.2);
        musicplayer.setAutoPlay(true);
        musicplayer.setCycleCount(MediaPlayer.INDEFINITE);
        musicplayer.play();
    }
    public void muteBackgroundMusic(){
        musicplayer.stop();
    }


    @Override
    public boolean isResizable()
    {
        return true;
    }

    public double prefWidth(double height) {
        Screen screen = Screen.getPrimary();
        return screen.getVisualBounds().getWidth()-50;
    }

    @Override
    public double prefHeight(double width) {
        Screen screen = Screen.getPrimary();
        return screen.getVisualBounds().getHeight()-50;
    }
    /*
    @Override
    public void resize(double width, double height)
    {
        screen = Screen.getPrimary();
        screen_height = screen.getVisualBounds().getHeight()-50;
        screen_width = screen.getVisualBounds().getWidth()-20;
        super.setWidth(screen_width);
        super.setHeight(screen_height);
        draw();
    }
    *
     */



}
