package View;

import ViewModel.MyViewModel;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.Observable;
import java.util.Observer;

public class FileHandleWindow implements Observer {

    private File user_maze_file;
    private MyViewModel vm;
    private FileChooser fileChooser;


    public FileHandleWindow(MyViewModel viewModel,boolean load_save) {
        this.vm = viewModel;
        Stage window = new Stage();
        fileChooser = new FileChooser();
        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        if(load_save)
            //Show save file dialog
            user_maze_file = fileChooser.showSaveDialog(window);
        else
            //show open file dialog
            user_maze_file = fileChooser.showOpenDialog(window);

    }



    public void loadTextFile()  {
        try {
            vm.loadMaze(user_maze_file);
        } catch (IOException | ClassNotFoundException e) {
            showAlert("Could not find file: " + e.getMessage());
        }
    }


    public void saveTextToFile() {
        try {
            if (vm.saveMaze(user_maze_file))
                showAlert("Maze data successfully saved!");
        }
        catch (FileNotFoundException f){
            showAlert("Problem saving the file: \n" + f.getMessage());
        } catch (IOException e) {
            showAlert("Can't write the data to file: \n" + e.getMessage());
        }
    }

    private void showAlert(String message)  {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(message);;
        alert.show();
    }

    public void setViewModel(MyViewModel viewModel) {
        vm=viewModel;
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
