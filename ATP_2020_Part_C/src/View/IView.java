package View;

import ViewModel.MyViewModel;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public interface IView extends Observer  {

    public void setViewModel(MyViewModel viewModel);

    @Override
    default void update(Observable o, Object arg) {
        //TOBE implemented by each view
    }

    default void handleMenuNew() throws IOException{//TOBE implemented by each view
    };
    public void handleMenuSave() throws IOException;
    public void handleMenuLoad();
    public void handleMenuExit();
    public void handleMenuGameRules();
    public void handleMenuSymbols();
    public void handleMenuProperties();
    public void handleMenuAlgorithmUsed();
    public void handleMenuAboutTheProgrammers();
    public void keyPressed(KeyEvent keyEvent);
    public void mouseClicked(MouseEvent mouseEvent);
    public void showSolution(ActionEvent actionEvent);
    public void hideSolution(ActionEvent actionEvent);
    public void gameWon();

    }
