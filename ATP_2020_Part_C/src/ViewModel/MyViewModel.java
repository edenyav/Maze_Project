package ViewModel;

import Model.FinishException;
import Model.IModel;
import algorithms.mazeGenerators.Maze;
import algorithms.mazeGenerators.Position;
import algorithms.search.AState;
import algorithms.search.MazeState;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;

import javafx.scene.input.KeyEvent;
import java.io.*;
import java.nio.file.FileSystemNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Observable;
import java.util.Observer;

public class MyViewModel extends Observable implements Observer {


    private IModel model;

    private StringProperty maze_rows, maze_cols;


    @Override
    public void update(Observable o, Object arg) {
        this.setChanged();
        if(arg instanceof int[])
            this.notifyObservers((int[])arg);
        else if(arg instanceof Position[])
            this.notifyObservers((Position[])arg);
        else if(arg instanceof Maze)
            this.notifyObservers((Maze)arg);
    }


    public MyViewModel(IModel model) {
        this.model = model;
    }

    public void updatePlayerPosition(KeyEvent keyEvent) throws FinishException {
        String direction = "";
        switch (keyEvent.getCode()) {
            case NUMPAD2: // Move player DOWN
                direction = "DOWN";
                break;
            case NUMPAD8: // Move player UP
                direction = "UP";
                break;
            case NUMPAD6: // Move player RIGHT
                direction = "RIGHT";
                break;
            case NUMPAD4: // Move player LEFT
                direction = "LEFT";
                break;
            case NUMPAD1: // Move player DOWN and LEFT
                direction = "DOWN_LEFT";
                break;
            case NUMPAD3: // Move player DOWN and RIGHT
                direction = "DOWN_RIGHT";
                break;
            case NUMPAD7: // Move player UP and LEFT
                direction = "UP_LEFT";
                break;
            case NUMPAD9: // Move player UP and RIGHT
                direction = "UP_RIGHT";
                break;
            default: // Don't move player
                direction = "STAY";
                break;
        }
        model.setPlayerLocation(direction);

    }


    /*
    The binding method for the maze rows property
     */
    public void setMaze_rows(StringProperty maze_rows) {
        this.maze_rows = maze_rows;
    }

    /*
    The binding method for the maze columns property
     */
    public void setMaze_cols(StringProperty maze_cols) {
        this.maze_cols = maze_cols;
    }

    /*
    The binding method for the maze difficulty property
     */
    public void setMaze_difficulty(String maze_difficulty) {
        if(maze_difficulty.equals("Easy Maze"))
            maze_difficulty = "SimpleMazeGenerator";
        else
            maze_difficulty = "MyMazeGenerator";

        model.setGameDifficulty(maze_difficulty);
    }


    /*
        The binding method for the maze.
        The ViewModel is the observable and when the maze changes it notifies all
        the observers (Views) of the change.
         */
    public void createGameForView() throws InputMismatchException {
        if(maze_cols == null || maze_rows == null)
            throw new InputMismatchException();

        int int_maze_rows = validateInput(maze_rows.getValue());
        int int_maze_cols = validateInput(maze_cols.getValue());
        //this.setChanged();
        //this.notifyObservers((Maze)model.createGame(int_maze_rows,int_maze_cols));
        model.createGame(int_maze_rows,int_maze_cols);
    }

    public ArrayList<String> showSolutionViewModel(ActionEvent actionEvent) throws NullPointerException{
        ArrayList<AState> state_path = model.getSolution().getSolutionPath();
        ArrayList<String> direction_path = new ArrayList<>();
        MazeState current = (MazeState)state_path.get(0);
        for (int i = 1; i <state_path.size() ; i++) {
            MazeState next = (MazeState) state_path.get(i);
            int next_row = next.getPosition().getRowIndex();
            int next_col = next.getPosition().getColumnIndex();

            //move up
            if(current.getPosition().getRowIndex()-1==next_row && current.getPosition().getColumnIndex()==next_col)
                direction_path.add("UP");
                //move up and right
            else if(current.getPosition().getRowIndex()-1==next_row && current.getPosition().getColumnIndex()+1==next_col)
                direction_path.add("UP_RIGHT");
                //move right
            else if(current.getPosition().getRowIndex()==next_row && current.getPosition().getColumnIndex()+1==next_col)
                direction_path.add("RIGHT");
                //move down and right
            else if(current.getPosition().getRowIndex()+1==next_row && current.getPosition().getColumnIndex()+1==next_col)
                direction_path.add("DOWN_RIGHT");
                //move down
            else if(current.getPosition().getRowIndex()+1==next_row && current.getPosition().getColumnIndex()==next_col)
                direction_path.add("DOWN");
                //move down and left
            else if(current.getPosition().getRowIndex()+1==next_row && current.getPosition().getColumnIndex()-1==next_col)
                direction_path.add("DOWN_LEFT");
                //move left
            else if(current.getPosition().getRowIndex()==next_row && current.getPosition().getColumnIndex()-1==next_col)
                direction_path.add("LEFT");
                //move up and left
            else if(current.getPosition().getRowIndex()-1==next_row && current.getPosition().getColumnIndex()-1==next_col)
                direction_path.add("UP_LEFT");
            current = next;        }
        return direction_path;
    }

    public boolean saveMaze(File file) throws FileNotFoundException,IOException {
        if(file == null)
            throw new FileSystemNotFoundException();

        WriteToFile(file.getAbsolutePath(),model.getCompressedMaze());
        return true;
    }

    public void loadMaze(File file) throws IOException, ClassNotFoundException {

        FileInputStream fileIn = new FileInputStream(file.getAbsoluteFile());
        ObjectInputStream objectIn = new ObjectInputStream(fileIn);
        Object obj = objectIn.readObject();
        objectIn.close();

        model.loadGame((byte[])obj);
    }

    public synchronized void WriteToFile(String path, Object data) throws IOException {

        FileOutputStream fileOut = new FileOutputStream(path);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
        objectOut.writeObject(data);
        fileOut.flush();
        fileOut.close();
    }

    private boolean isNumeric(String str){
        return str.chars().allMatch( Character::isDigit );
    }

    private int validateInput(String input) throws InputMismatchException{
        if(!isNumeric(input))
            throw new InputMismatchException();
        int user_int_input = Integer.valueOf(input);
        if(user_int_input<=0)
            throw new InputMismatchException();
        return user_int_input;
    }
}
